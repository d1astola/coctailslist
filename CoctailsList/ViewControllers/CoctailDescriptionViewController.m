//
//  CoctailDescriptionViewController.m
//  CoctailsList
//
//  Created by Tester on 3/22/18.
//  Copyright © 2018 Igor Pak. All rights reserved.
//

#import "CoctailDescriptionViewController.h"
#import <SDWebImage/UIImageView+WebCache.h>

@interface CoctailDescriptionViewController ()

@property (weak, nonatomic) IBOutlet UIImageView *coctailImageView;
@property (weak, nonatomic) IBOutlet UILabel *coctailTitleLabel;
@property (weak, nonatomic) IBOutlet UITextView *coctailRecipeList;


@end

@implementation CoctailDescriptionViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.coctailTitleLabel.text = [NSString stringWithFormat:@"%@ (%@, %@)", self.coctail.name, self.coctail.category, self.coctail.alcoholType];
    self.coctailRecipeList.text = self.coctail.recipe;
    NSURL* imageURL = [NSURL URLWithString:self.coctail.pictureURL];
    [self.coctailImageView sd_setImageWithURL:imageURL];
}

- (void) viewWillAppear:(BOOL)animated
{
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    IngredientsListViewController* vc = [segue destinationViewController];
    vc.coctail = self.coctail;
}

- (IBAction)showIngredientsList:(UIButton *)sender {
    
    [self performSegueWithIdentifier:@"showIngredientsList" sender:self];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
