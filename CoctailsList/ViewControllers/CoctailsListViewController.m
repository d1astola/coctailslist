//
//  ViewController.m
//  CoctailsList
//
//  Created by Tester on 3/22/18.
//  Copyright © 2018 Igor Pak. All rights reserved.
//

#import "CoctailsListViewController.h"
#import "CoctailsFRC.h"

@interface CoctailsListViewController () <UITableViewDelegate, UITableViewDataSource, NetworkControllerDelegate, CoctailFRCDelegate>

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) NetworkController* netController;
@property (strong, nonatomic) CoctailsCoreDataController* coctailsCoreData;
@property (strong, nonatomic) CoctailsFRC* coctailFRC;
@property (strong, nonatomic) Coctail* currentCoctail;

- (IBAction)addButton:(UIBarButtonItem *)sender;

@end

@implementation CoctailsListViewController 

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    self.netController = [[NetworkController alloc] init];
    self.netController.delegate = self;
    self.coctailsCoreData = [[CoctailsCoreDataController alloc] init];
}

- (void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.tableView reloadData];
}

- (void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    CoctailDescriptionViewController* vc = [segue destinationViewController];
    vc.coctail = self.currentCoctail;
}

- (CoctailsFRC *)coctailFRC
{
    if (_coctailFRC == nil)
    {
        _coctailFRC = [CoctailsFRC new];
        _coctailFRC.delegate = self;
    }
    return _coctailFRC;
}

#pragma mark - UITableViewDataSource

- (NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {

    NSInteger countObjects = [self.coctailFRC countObjects];
    return countObjects;
}

- (UITableViewCell*) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {

    UITableViewCell* cell = [tableView dequeueReusableCellWithIdentifier:@"DetailCell"];
    Coctail* coctail = [self.coctailFRC objectAtIndexPath:indexPath];

    cell.textLabel.text = coctail.name;
    cell.detailTextLabel.text = coctail.category;
    return cell;
}

#pragma mark - NetworkControllerDelegate

- (void) loadSuccessfullyWithResult:(NSDictionary *)dict {

    [self.coctailsCoreData addObject:dict];
}

- (void)loadingError {

    UIAlertController* alert = [UIAlertController alertControllerWithTitle:@"Error"
                                                                   message:@"AFNetworking error"
                                                            preferredStyle:UIAlertControllerStyleAlert];

    UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
                                                          handler:^(UIAlertAction * action) {}];

    [alert addAction:defaultAction];
    [self presentViewController:alert animated:YES completion:nil];
}

#pragma mark - UITableViewDelegate

- (void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {

    Coctail* coctail = [self.coctailFRC objectAtIndexPath:indexPath];
    self.currentCoctail = coctail;
    [self performSegueWithIdentifier:@"showCoctailDescription" sender:self];
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

- (NSArray<UITableViewRowAction *> *)tableView:(UITableView *)tableView editActionsForRowAtIndexPath:(NSIndexPath *)indexPath {

    Coctail* coctail = [self.coctailFRC objectAtIndexPath:indexPath];
    UITableViewRowAction* action = [UITableViewRowAction rowActionWithStyle:UITableViewRowActionStyleDestructive
                                                                      title:@"Delete"
                                                                    handler:^(UITableViewRowAction * _Nonnull action, NSIndexPath * _Nonnull indexPath) {
                                                                        [self.coctailsCoreData deleteObjectByName:coctail.name];
                                                                    }];

    return @[action];
}

#pragma mark - CoctailsFRCDelegate

- (void) dataDidUpdate
{
    [self.tableView reloadData];
}

#pragma mark - Actions

- (IBAction)addButton:(UIBarButtonItem *)sender {

    [self.netController getJSONFromURL:@"https://www.thecocktaildb.com/api/json/v1/1/random.php"];
}

@end
