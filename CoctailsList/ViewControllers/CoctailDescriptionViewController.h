//
//  CoctailDescriptionViewController.h
//  CoctailsList
//
//  Created by Tester on 3/22/18.
//  Copyright © 2018 Igor Pak. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Coctail+CoreDataClass.h"
#import "IngredientsListViewController.h"

@interface CoctailDescriptionViewController : UIViewController

@property (strong, nonatomic) Coctail* coctail;

@end
