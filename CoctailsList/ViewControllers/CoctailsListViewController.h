//
//  ViewController.h
//  CoctailsList
//
//  Created by Tester on 3/22/18.
//  Copyright © 2018 Igor Pak. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NetworkController.h"
#import "CoctailsCoreDataController.h"
#import "CoctailDescriptionViewController.h"

@interface CoctailsListViewController : UIViewController 


@end

