//
//  NetworkController.m
//  CoctailsList
//
//  Created by Tester on 3/22/18.
//  Copyright © 2018 Igor Pak. All rights reserved.
//

#import "NetworkController.h"

@interface NetworkController ()

@property (strong, nonatomic) AFHTTPSessionManager* sessionManager;

@end

@implementation NetworkController

- (id) init {
    
    self = [super init];
    if(self)
    {
        self.sessionManager = [AFHTTPSessionManager manager];
    }
    return self;
}

- (void) getJSONFromURL:(NSString *)url {
    
    [self.sessionManager GET:url
                  parameters:nil
                    progress:nil
                     success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
                         if ([responseObject isKindOfClass:[NSDictionary class]]) {
                             NSArray* array = [responseObject objectForKey:@"drinks"];
                             NSDictionary* dict = [array objectAtIndex:0];
                             [self.delegate loadSuccessfullyWithResult:dict];
                         }
                         else {
                             [self.delegate loadingError];
                         }
                     }
                     failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                         [self.delegate loadingError];
                     }];
}

@end
