//
//  CoctailsFRC.h
//  CoctailsList
//
//  Created by Tester on 3/22/18.
//  Copyright © 2018 Igor Pak. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CoctailsCoreDataController.h"

@protocol CoctailFRCDelegate
- (void) dataDidUpdate;
@end

@interface CoctailsFRC : NSObject <NSFetchedResultsControllerDelegate>

@property (weak, nonatomic) id <CoctailFRCDelegate> delegate;

- (NSArray*) allObjects;
- (Coctail*) objectAtIndexPath: (NSIndexPath*) indexPath;
- (NSInteger) countObjects;

@end
