//
//  CoctailsCoreDataController.h
//  CoctailsList
//
//  Created by Tester on 3/22/18.
//  Copyright © 2018 Igor Pak. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <MagicalRecord/MagicalRecord.h>
#import "Coctail+CoreDataClass.h"
#import "Ingredient+CoreDataClass.h"

@interface CoctailsCoreDataController : NSObject

- (void) addObject: (NSDictionary*) dict;
- (void) deleteObjectByName: (NSString*) name;

@end
