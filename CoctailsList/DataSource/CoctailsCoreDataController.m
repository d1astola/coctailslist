//
//  CoctailsCoreDataController.m
//  CoctailsList
//
//  Created by Tester on 3/22/18.
//  Copyright © 2018 Igor Pak. All rights reserved.
//

#import "CoctailsCoreDataController.h"
#import "CoctailDescription.h"

@interface CoctailsCoreDataController ()

@end

@implementation CoctailsCoreDataController

#pragma mark - Private Methods

- (void) setIngredientsTo: (Coctail*) coctail from: (NSDictionary*) dictionary inContext: (NSManagedObjectContext*) context {
    
    NSString* string;
    NSInteger i = 1;
    do {
        Ingredient* ingredient = [Ingredient MR_createEntityInContext:context];
        ingredient.ingredient = [dictionary objectForKey:[NSString stringWithFormat:@"strIngredient%ld", i]];
        ingredient.measure = [dictionary objectForKey:[NSString stringWithFormat:@"strMeasure%ld", i]];
        ingredient.creationDate = [NSDate date];
        [coctail addIngredientObject:ingredient];
        
        i++;
        string = [dictionary objectForKey:[NSString stringWithFormat:@"strIngredient%ld", i]];
    } while ([string length] != 0);
}

#pragma mark - SET

- (void) addObject: (NSDictionary*) dict {
    
    CoctailDescription* coctailDescription = [CoctailDescription new];
    [coctailDescription configureWithDictionary:dict];
    [MagicalRecord saveWithBlock:^(NSManagedObjectContext * _Nonnull localContext) {
        Coctail* coctail = [Coctail MR_findFirstByAttribute:@"name" withValue:[dict objectForKey:@"strDrink"]];
        if (coctail == nil)
        {
            coctail = [Coctail MR_createEntityInContext:localContext];
        }
        
        coctail.creationDate =  [NSDate date];
        coctail.name =          coctailDescription.name;
        coctail.category =      coctailDescription.category;
        coctail.alcoholType =   coctailDescription.alcoholType;
        coctail.recipe =        coctailDescription.recipe;
        coctail.pictureURL =    coctailDescription.pictureurl;
        
        [self setIngredientsTo:coctail from:dict inContext:localContext];
    }
                      completion:^(BOOL contextDidSave, NSError * _Nullable error) {
                      }];
}

#pragma mark - DEL

- (void) deleteObjectByName:(NSString *)name
{
    [MagicalRecord saveWithBlock:^(NSManagedObjectContext * _Nonnull localContext) {
        Coctail* coctail = [Coctail MR_findFirstByAttribute:@"name" withValue:name];
        NSArray* array = [coctail.ingredient allObjects];
        for (Ingredient* ing in array) {
            [ing MR_deleteEntityInContext:localContext];
        }
        [coctail MR_deleteEntityInContext:localContext];
    }
                      completion:^(BOOL contextDidSave, NSError * _Nullable error) {
                      }];
}

@end
