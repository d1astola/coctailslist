//
//  Coctail.m
//  CoctailsList
//
//  Created by Tester on 3/23/18.
//  Copyright © 2018 Igor Pak. All rights reserved.
//

#import "CoctailDescription.h"

@implementation CoctailDescription

- (void) configureWithDictionary: (NSDictionary*) dict
{
    self.name =         [dict objectForKey:@"strDrink"];
    self.category =     [dict objectForKey:@"strCategory"];
    self.alcoholType =  [dict objectForKey:@"strAlcoholic"];
    self.recipe =       [dict objectForKey:@"strInstructions"];
    self.pictureurl =   [dict objectForKey:@"strDrinkThumb"];
}

@end
