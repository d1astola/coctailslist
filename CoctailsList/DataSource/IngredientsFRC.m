//
//  IngredientsFRC.m
//  CoctailsList
//
//  Created by Tester on 3/22/18.
//  Copyright © 2018 Igor Pak. All rights reserved.
//

#import "IngredientsFRC.h"

@implementation IngredientsFRC

- (id) init {
    
    self = [super init];
    self.frc = [Ingredient MR_fetchAllSortedBy:@"creationDate"
                                  ascending:YES
                              withPredicate:nil
                                    groupBy:nil
                                   delegate:self];
    return self;
}

- (void) controller:(NSFetchedResultsController *)controller didChangeObject:(nonnull id)anObject atIndexPath:(nullable NSIndexPath *)indexPath forChangeType:(NSFetchedResultsChangeType)type newIndexPath:(nullable NSIndexPath *)newIndexPath {
    
}

@end
