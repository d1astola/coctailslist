//
//  Coctail.h
//  CoctailsList
//
//  Created by Tester on 3/23/18.
//  Copyright © 2018 Igor Pak. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CoctailDescription : NSObject

@property (strong, nonatomic) NSString* name;
@property (strong, nonatomic) NSString* category;
@property (strong, nonatomic) NSString* alcoholType;
@property (strong, nonatomic) NSString* recipe;
@property (strong, nonatomic) NSString* pictureurl;

- (void) configureWithDictionary: (NSDictionary*) dict;

@end
