//
//  IngredientsFRC.h
//  CoctailsList
//
//  Created by Tester on 3/22/18.
//  Copyright © 2018 Igor Pak. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "IngredientsCoreDataController.h"

@interface IngredientsFRC : NSObject <NSFetchedResultsControllerDelegate>

@property (strong, nonatomic) NSFetchedResultsController* frc;

@end
