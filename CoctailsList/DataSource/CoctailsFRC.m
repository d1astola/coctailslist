//
//  CoctailsFRC.m
//  CoctailsList
//
//  Created by Tester on 3/22/18.
//  Copyright © 2018 Igor Pak. All rights reserved.
//

#import "CoctailsFRC.h"

@interface CoctailsFRC ()

@property (strong, nonatomic) NSFetchedResultsController* frc;

@end

@implementation CoctailsFRC

- (id) init {
    
    self = [super init];
    self.frc = [Coctail MR_fetchAllSortedBy:@"creationDate"
                                  ascending:YES
                              withPredicate:nil
                                    groupBy:nil
                                   delegate:self];
    return self;
}

- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller {
    
    [self.delegate dataDidUpdate];
}

- (NSArray*) allObjects {
    
    return [self.frc fetchedObjects];
}

- (Coctail *)objectAtIndexPath:(NSIndexPath *)indexPath {
    
    return [self.frc objectAtIndexPath:indexPath];
}

- (NSInteger) countObjects
{
    
    return [[self.frc fetchedObjects] count];
}

@end
