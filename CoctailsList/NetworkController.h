//
//  NetworkController.h
//  CoctailsList
//
//  Created by Tester on 3/22/18.
//  Copyright © 2018 Igor Pak. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AFNetworking.h>

@protocol NetworkControllerDelegate;

@interface NetworkController : NSObject

- (void) getJSONFromURL: (NSString*) url;

@property (weak, nonatomic) id <NetworkControllerDelegate> delegate;

@end

@protocol NetworkControllerDelegate

@required
- (void) loadSuccessfullyWithResult: (NSDictionary*) dict;
- (void) loadingError;

@end
